const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'assets/js')
   .sass('resources/sass/style.scss', 'assets/css')
   //.sass('resources/sass/comingsoon.scss', 'assets/css')
   //.sass('resources/sass/comingsoon-shop.scss', 'assets/css');